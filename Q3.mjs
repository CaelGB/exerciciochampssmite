import { gods } from "./deuses.mjs";

gods.sort((a, b) => {
    let x = a.pantheon.toLowerCase();
    let y = b.pantheon.toLowerCase();

    if(x > y){
        return 1;
    } 
    else if(x < y){
        return -1;
    }
    return 0;
})
console.log(gods)